var AnimeBlock = {
	info: null,
	blocks: null,
	container: null,
	infoContent: null,
	
	init: function() {
		var i;
		this.info = document.getElementById('anime_info');
		this.infoContent = document.getElementById('anime_info_content');
		this.container = document.getElementById('dle-content');
		this.blocks = $(this.container).children('[data-type="anime"]');
		for (i = this.blocks.length - 1; i >= 0; i--) {
			this.prepareBlock(this.blocks[i], i);
		}					
	},
	
	prepareBlock: function(block, position) {
		var place = position + (4 - position % 4),
			id = block.getAttribute('data-anime-list'),
			info = block.querySelector('script[type="x-template"][data-anime-type="info"]').innerHTML,
			self = this,
      rating = block.getAttribute('rating'),
      rating_block = $(block).children('.anime_post_in').children('.m-rating-meter');
      if(parseFloat(rating) < 1)
        rating_block.css('display','none');
  		block.onclick = function() {
        $('.anime_info').css("display","inline-block");
  			self.infoContent.innerHTML = info;
  			self.container.insertBefore(self.info, self.blocks[place]);
        var category = $('.genres_tags > a:first-of-type').attr('href').replace(/http:\/\/(.*?)\/(.*?)\//, '$2');
        var music = null;
        var usefilenames = false;
        switch(category){
          case "amv": 
            usefilenames = true; music = false; break;
          case "anime": 
            music = false; break;
          case "ost": 
            music = true; break;
        }
        if(music != null){
    		  $('.watch_button_info').html(music?'Cлушать':'Смотреть');
          player.install(
            '#player', 
            '.vk_multifilm', 
            false, 
            music, 
            true,
            usefilenames
          )
        };
		  };
	}
};

/*
window.onload = function() {
	AnimeBlock.init();
};
*/

$(document).ready(function() {
	AnimeBlock.init();
});
