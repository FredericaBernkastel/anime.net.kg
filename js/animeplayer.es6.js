let DEBUG = false;
let domain = 'http://anime.net.kg';
let data_domain = 'http://data.anime.net.kg';
//#implementation

function strpos(haystack, needle, offset) {
    let i = haystack.indexOf(needle, offset);
    return i >= 0 ? i : false;
}
function flatten(arr) {
  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten);
  }, []);
}

navigator.sayswho= (function(){
    let ua= navigator.userAgent, tem,
    M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
    if(/trident/i.test(M[1])){
        tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
        return 'IE '+(tem[1] || '');
    }
    if(M[1]=== 'Chrome'){
        tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
        if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
    }
    M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
    if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
    return M.join(' ');
})();

String.prototype.ReplaceMultiple = function(obj) {
    var retStr = this;
    for (var x in obj) {
        retStr = retStr.replace(new RegExp(x, 'g'), obj[x]);
    }
    return retStr;
};

// Speed up calls to hasOwnProperty
var hasOwnProperty = Object.prototype.hasOwnProperty;

function isEmpty(obj) {

    // null and undefined are "empty"
    if (obj == null) return true;

    // Assume if it has a length property with a non-zero value
    // that that property is correct.
    if (obj.length > 0)    return false;
    if (obj.length === 0)  return true;

    // If it isn't an object at this point
    // it is empty, but it can't be anything *but* empty
    // Is it empty?  Depends on your application.
    if (typeof obj !== "object") return true;

    // Otherwise, does it have any properties of its own?
    // Note that this doesn't handle
    // toString and valueOf enumeration bugs in IE < 9
    for (var key in obj) {
        if (hasOwnProperty.call(obj, key)) return false;
    }

    return true;
}

let player = {};

player.animelistp = function(){
  let self = player;
  let urls = "";
  let lastfolderpos = 9999;
  if($('#video-ids').exists())
    if($('#video-ids').html().length > 0)
      urls = $('#video-ids').html().split(",")
    else{
      self.animelistp_onAjaxSuccess("");
      return false;
    }
    
  for(let id = 0; id < urls.length; id++)
    if(!self.regex.blive.test(urls[id]) && !self.regex.youtube.test(urls[id]) && !self.regex.section_descriptor.test(urls[id]) && !self.regex.grabURI.test(urls[id]) && !self.regex.disabledch.test(urls[id])){
      self.folders.push(urls[id]);
      lastfolderpos = id;
    }
  if(self.doshow){
    if(self.folders.length == 0){
      self.animelistp_onAjaxSuccess(urls);
      return true;
    }
    let reqcounter = 0;
    var receivedcounter = 0;
    for(let id = 0; (id < urls.length) && (id <= lastfolderpos); id++){
      if(!self.regex.blive.test(urls[id]) && !self.regex.youtube.test(urls[id]) && !self.regex.section_descriptor.test(urls[id]) && !self.regex.grabURI.test(urls[id]) && !self.regex.disabledch.test(urls[id])){
        if(reqcounter <= self.folders.length){
          reqcounter++;
          let url = `${data_domain}/phpscript/player.scandir.php?url=${urls[id]}&id=${id}&reqcounter=${reqcounter + (self.music?"&music=true":"")}&user=${self.user}`; 
          $.ajax({
            type:  "GET",
            url:   url,
            async: !DEBUG,
            success : function(data) {
                receivedcounter++;
            		data = jQuery.parseJSON(data); 
              	let i = 0;
                if(data.msg)
                  self.errors += data.msg;
              	urls[data.id] = new Array();
              	while(data[i] != undefined){
              	  urls[data.id].push(data[i]); 
              	  i++;
              	};
                self.loadingProgress = receivedcounter / self.folders.length * 100;
                	if((receivedcounter == self.folders.length)){
                  	self.animelistp_onAjaxSuccess(flatten(urls)); 
                  }
            }
          });
        }
        else
          return true;
      };
        /*$.get(
          "/phpscript/scandir.php",
          {
            "url": ".." + urls[id],
            "id": id
          },
          function(data){
            data = jQuery.parseJSON(data); 
            urls[data.id] = data.data.split(",");
          }
        )*/
    };
  }
  else
    self.animelistp_onAjaxSuccess({});
  return false;
};

player.setChapterSelector = function(){
  let html_ = "";
  let class_ = '';
  let alt = '';
  for(let i = 0; i < this.chapters.length; i++){  
    let i_ = this.ongoing ? this.chapters.length - i - 1 : i;
    if(this.regex.disabledch.test(this.ids[i_].path))
      continue
    else if(!(this.regex.notEmpty.test(this.ids[i_].path))){
      class_ = "disabled"; 
      alt = "Серия отсутствует";
      this.chapters[i_].caption += " (отсутствует)"; 
    }
    else if(this.regex.testsect.test(this.ids[i_]))
      {class_ = "section"; alt = "";}
    else
      {class_ = ""; alt = ""};
    html_ += `<li data-src="${this.chapters[i_].configURI}" ${class_?'class="' + class_ + '"':''} ${alt?'title="' + alt + '"':''} index="${i_}">${this.chapters[i_].caption}</li>`;      
  };
  $('#serial_selector').html(html_);   
};

player.telemetry = function(index){
  if (this.user != 'guest' && this.user != undefined) {
    var url = '/phpscript/player.scandir.php?user=' + this.user + '&news_id=' + this.news_id + '&last_chapter=true&chapter_index=' + index;
    $.ajax({
      type: "PUT",
      url: url,
      async: !DEBUG
    });
  }
}

player.grabURI = function(URI, index){
  //direct link
  if(this.regex.grabURI.test(URI)){
    let data = {};
    data.playlist = [{url:"[xfvalue_screenshot1]"}, {url:URI}];
    this.show(data,2,index);
  }
  //blive embed
  else if(URI.indexOf(this.blive_embedURI) == 0)
    $.get(
      data_domain + "/phpscript/player.proxy.php",
      {
        "url": URI,
        "user": this.user
      },
      function(data){
        data = data.replace(/'/g,'"');
        data = jQuery.parseJSON(data); 
        player.show(data,1,index);
      }
    ) 
  //youtube embed
  else if(URI.indexOf(this.youtube_embedURI) == 0){
    player.show(URI, 3, index);
  }
};

player.hide = function(affectCSS){
  var self = this;
  if(this.videoJS){
    try{
      this.tmp.vjs_audiobtns = {};
      this.videoJS.dispose();
    }
    catch(e){
      console.log(e.message);
    }
  }
  $(this.container).children().filter("audio").each(function () {
    this.pause(); // can't hurt
    this.src = '';
    this.load();
    delete this; // @sparkey reports that this did the trick (even though it makes no sense!)
    $(this).remove(); // this is probably what actually does the trick
  });
  $(this.container).children().filter("video").each(function () {
    this.pause(); // can't hurt
    this.src = '';
    this.load();
    delete this; // @sparkey reports that this did the trick (even though it makes no sense!)
    $(this).remove(); // this is probably what actually does the trick
  });
  $(this.container).empty();
  if(affectCSS){
    $(this.wrapper).css('display', 'none');
    $('.wrapper, .footer, #header_bg').each(function(){
      $(this).css({
          'display':'block'
      });
    });
    if(self.doshow && self.popup){
      $(self.wrapper).detach().appendTo('#anime_info_content');
      $(window).scrollTop(this.windowScroll);
    };
  };
};

player.nextChapter = function(el){
  if(!el)
    el = $('#serial_selector > li.pcurrent')
  else
    el = $(el);
  if(this.ongoing)
    el.prev().click()
  else
    el.next().click()
}

player.show = function(data,type,index){
  let self = this;
  this.hide(false);
  this.currentChapterIdx = index;
  if(index != this.last_chapter)
    this.telemetry(index);
  $(this.wrapper).css("display", "table");
  this.Sources.length = 0;
  switch(type){    
        case 1: 
          this.Sources.push({file: data.playlist[1].url});
          break;  
        case 2: 
          this.Sources.push({file: data.playlist[1].url});
          break;  
  };
  let id3;
  if((this.ids[index].id3 != undefined) && (this.ids[index].id3 != null)){
    id3 = this.ids[index].id3.tags;
    if((id3 != undefined) && (id3 != null)){
      id3.bitrate = ((this.ids[index].id3.bitrate != undefined)?
                        Math.round(this.ids[index].id3.bitrate / 1000).toString()+' ['
                        :'&nbsp') 
                    + (this.ids[index].id3.audio.bitrate_mode?
                        this.ids[index].id3.audio.bitrate_mode
                        :'') 
                    + ']';
    }
    else
      id3 = {};
  }
  else 
    id3 = {};
  let musicinfo = '';
  if ((id3!==void 0) && this.music) 
    musicinfo = `
      <center>
        <table class="ostplayerinfo" border="1px" cellpadding="2" cellspacing="1">
          <tbody>
            <tr>
              <td class="ostplayerinfo" colspan="2" align="center">
                <b>Общая информация</b>
              </td></tr>
            <tr>
              <td class="ostplayerinfo" align="right">
                <b>Сейчас в играет:</b>
              </td>
              <td class="ostplayerinfo" align="center" id="ost-title" style="color:red; text-shadow: 0px 0px 5px rgb(114, 0, 255),0px 5px 5px rgb(114, 0, 254),0px -5px 5px rgb(114, 0, 255),5px 0px 5px rgb(114, 0, 255),-5px 0px 5px rgb(114, 0, 255); font-weight: bold;">
                ${(id3.title !== void 0)?id3.title[0]:'&nbsp;'}
              </td>
            </tr>
            <tr>
              <td class="ostplayerinfo" align="right">
                <b>Артист:</b>&nbsp;
              </td>
              <td class="ostplayerinfo" align="center" id="ost-artist">
                ${(id3.artist !== void 0)?id3.artist[0]:'&nbsp;'}
              </td>
            </tr>
            <tr>
              <td class="ostplayerinfo" align="right">
                <b>Альбом:</b>&nbsp;
              </td>
              <td class="ostplayerinfo" align="center" id="ost-album">
                ${(id3.album !== void 0)?id3.album[0]:'&nbsp;'}
              </td>
            </tr>
            <tr>
              <td class="ostplayerinfo" align="right">
                <b>Год:</b>&nbsp;
              </td>
              <td align="center" class="ostplayerinfo" id="ost-year">
                ${(id3.year !== void 0)?id3.year[0]:'&nbsp;'}
              </td>
            </tr>
            <tr>
              <td class="ostplayerinfo" align="right">
                <b>Жанр:</b>&nbsp;
              </td>
              <td align="center" class="ostplayerinfo" id="ost-genre">
                ${(id3.genre !== void 0)?id3.genre[0]:'&nbsp;'}
              </td>
            </tr>
            <tr>
              <td class="ostplayerinfo" align="right">
                <b>Битрейт:</b>&nbsp;
              </td>
              <td align="center" class="ostplayerinfo" id="ost-bitrate" style="text-transform:uppercase">
                ${(id3.bitrate !== void 0)?id3.bitrate:'&nbsp;'}
              </td>
            </tr>
          </tbody>
        </table>
      </center>
    `;
  let playerHTML5 = '';
  if(data.playlist)
    playerHTML5 = `
      <${this.music?"audio":"video height='360' class='video-js vjs-sublime-skin' id='video'"} width="640" controls>
        <source src="${data.playlist[1].url}" id="src" type="${this.music?"audio/mpeg":"video/mp4"}">
        Плеер нер работает. Возможно, ваш браузер устарел.
      </${this.music?"audio":"video"}>
      ${(!this.music && !isEmpty(this.chapters[index].ext_audio))?'<audio id="audio" controls> \
         <source src="" type="audio/mp3"> \
      </audio>':''}
      ` + (this.music?musicinfo:"");
  let playerYoutube = `
    <iframe 
      id="ytplayer" 
      type="text/html" 
      width="640" 
      height="360"
      src="${data}?autoplay=${!this.firstShow}&origin=${domain}"
      frameborder="0"
      allowfullscreen
    />
  `;
  
  if(type == 3){ // youtube
    $(this.container).html(playerYoutube);
    this.firstShow = false;
  }
  else if((type < 3) && data.playlist){ //direct
    $(this.container).html(playerHTML5);
    //video.js
    if(!this.music){
      let plugins = {};
      if(this.chapters[index].subtitle)
        plugins = {
	          ass: {
	            'src': [this.chapters[index].subtitle],
							'enableSvg': false,
	            videoWidth: 640,
	            videoHeight: 360,
							button: false,
              renderer_enabled: this.enable_sub
	          }
	        };
      this.videoJS = videojs('video', {
        controls: true,
        //nativeControlsForTouch: false,
        width: 640,
        height: 360,
        fluid: false,
        language: 'ru',
        autoplay: !this.firstShow,
        flash: {
          swf: domain + '/templates/animewka/js/video.js/video-js.swf'
        },
        plugins: plugins
      });
      
      var videoJsButtonClass = videojs.getComponent('Button'),
        videoJsMenuButtonClass = videojs.getComponent('MenuButton'),
        videoJsMenuItemClass = videojs.getComponent('MenuItem'),
        vjs_audiobtns = {};
      var vjs_push_message = videojs.extend(videoJsButtonClass, {
      
        // The `init()` method will also work for constructor logic here, but it is 
        // deprecated. If you provide an `init()` method, it will override the
        // `constructor()` method!
        constructor: function() {
          videoJsButtonClass.call(this, self.videoJS);
        }, // notice the comma
      
        handleClick: function(){
          $('.vjs-push-message').css('display', 'none');
        }
      });
      
      var vjs_settings_button = videojs.extend(videoJsMenuButtonClass, {
      
        constructor: function() {
          videoJsMenuButtonClass.call(this, self.videoJS, {title: 'Настройки'});
        }, // notice the comma
      
        handleClick: function(){
          console.log('sett');
        }
      });
      
     var vjs_menu_enable_sub = videojs.extend(videoJsMenuItemClass, {
      
        constructor: function() {
          var label = "Субтитры: [нет]";
          if(self.chapters[index].subtitle)
            label = self.enable_sub?"Субтитры: [вкл]" : "Субтитры: [выкл]";
          videoJsMenuItemClass.call(this, self.videoJS, {selectable: true, label: label});
          if(self.chapters[index].subtitle)
            this.selected(self.enable_sub);
        }, // notice the comma
      
        handleClick: function(){
          if(!self.chapters[index].subtitle)
            return;
          self.enable_sub = !self.enable_sub;
          this.selected(self.enable_sub);
          this.el_.innerHTML = self.enable_sub?'Субтитры: [вкл]':'Субтитры: [выкл]';
          ass_renderer.setEnabled(self.enable_sub);
        }
      });
      
     var vjs_menu_ext_audio = videojs.extend(videoJsMenuItemClass, {
      
        constructor: function(id, label) {
          videoJsMenuItemClass.call(this, self.videoJS, {selectable: true, label: label});
          this.__id = id;
          this.el_.classList.add('align-left');
        }, // notice the comma
      
        handleClick: function handleClick() {
          if(self.ext_audio == this.__id)
            return;
          var old_id = self.ext_audio;
          self.ext_audio = this.__id;
          switch (this.__id) {
            case -1:
              break;
            case 0: // int
              player.tmp.vjs_audiobtns[0].selected(true);
              player.tmp.vjs_audiobtns[old_id].selected(false);
              player.videoJS.muted(false);
              player.tmp.ext_audio_el.pause();
              player.tmp.ext_audio_el.mute();
              break;
            default:
              player.tmp.vjs_audiobtns[old_id].selected(false);
              player.tmp.vjs_audiobtns[this.__id].selected(true);
              player.videoJS.muted(true);
              player.tmp.ext_audio_el.media.src = player.chapters[index].ext_audio[this.__id - 1].url;
              player.tmp.ext_audio_el.load();
              player.tmp.ext_audio_el.unmute();
              player.tmp.ext_audio_el.play();
              player.videoJS.play();
              break;
          }
        }
      });
      
      var vjs_menu_spacer = videojs.extend(videoJsMenuItemClass, {

        constructor: function constructor(label) {
          videoJsMenuItemClass.call(this, self.videoJS, { selectable: false, label: label?label:'&nbsp;' });
          if(label)
            this.el_.classList.add('vjs-menu-title')
          else
            this.el_.classList.add('vjs-menu-spacer')
        }, // notice the comma

        handleClick: function handleClick() {

        }
      });
      
      this.videoJS.ready(function(){
        this.on('play', () => {self.firstShow = false;});
        this.on('ended', () => {
          self.nextChapter();
        });
        this.on('volumechange', () => {
          if(self.chapters[index].ext_audio){
            if((self.ext_audio > 0) && self.chapters[index].ext_audio[self.ext_audio - 1]){
              if(this.volume() > 0){
                self.volume = this.volume();
                self.tmp.ext_audio_el.volume(self.volume);
                this.muted(true);
              }
            } 
          } else
            self.volume = this.volume()
        });
        this.volume(self.volume);
        
        if(!isEmpty(self.chapters[index].ext_audio) || self.chapters[index].subtitle){

          var vjs_settings_button_insatnce = self.videoJS.addChild(new vjs_settings_button());
          vjs_settings_button_insatnce.menu.addItem(new vjs_menu_enable_sub());
          if(!isEmpty(self.chapters[index].ext_audio)){
            vjs_settings_button_insatnce.menu.addItem(new vjs_menu_spacer());
            vjs_settings_button_insatnce.menu.addItem(new vjs_menu_spacer("Аудио"));
            self.tmp.vjs_audiobtns[0] = new vjs_menu_ext_audio(0, "[int]: RUS Sound");
            
            for(var _index in self.chapters[index].ext_audio) { 
              if (self.chapters[index].ext_audio.hasOwnProperty(_index)) {
                _index = parseInt(_index);
                self.tmp.vjs_audiobtns[_index + 1] = new vjs_menu_ext_audio(_index + 1, "[ext]: " + self.chapters[index].ext_audio[_index].label);
              }
            }
            
            for(var _index in self.tmp.vjs_audiobtns) { 
              if (self.tmp.vjs_audiobtns.hasOwnProperty(_index)) {
                try{
                  vjs_settings_button_insatnce.menu.addItem(self.tmp.vjs_audiobtns[_index]);
                }
                catch(e){
                  console.log(e.message);
                }
              }
            }
            
            if(self.tmp.vjs_audiobtns[self.ext_audio])
              self.tmp.vjs_audiobtns[self.ext_audio].selected(true);
          }
          vjs_settings_button_insatnce.addClass("vjs-settings-button");
          self.videoJS.controlBar.addChild(vjs_settings_button_insatnce);
          self.videoJS.controlBar.el().insertBefore(
            vjs_settings_button_insatnce.el(),
            self.videoJS.controlBar.getChild('customControlSpacer').el().nextSibling
          );
        }
        if(!isEmpty(self.chapters[index].ext_audio)){
          
          var vjs_push_message_insatnce = self.videoJS.addChild(new vjs_push_message());
          vjs_push_message_insatnce.addClass("vjs-push-message");
        
          self.tmp.ext_audio_el = Popcorn("#audio");
          $('#audio').css({'display' : 'none'});
          
          if(self.ext_audio > 0){
            if(self.chapters[index].ext_audio[self.ext_audio - 1]){
              self.videoJS.muted(true);
              self.tmp.ext_audio_el.media.src = self.chapters[index].ext_audio[self.ext_audio - 1].url;
              self.tmp.ext_audio_el.load();
              self.tmp.ext_audio_el.unmute();
            } else { //fallback to internal
              //self.ext_audio = 0;
            }
          }
          
				  var loadCount = 0,
            seek_count = 0,
            lasttime = 0,
            audioLasttime = 0,
            events = "play pause timeupdate seeking volumechange".split(/\s+/g),
            audioMutex = false;
        
          function log(msg, desync = -1){
            if(!msg)
              msg = 'popcornJS:: desync correction: ' + (desync * 1000).toFixed(3) + 'ms';
            console.log(msg);
          }
    
          function push_message(id){
            var message = $('.vjs-push-message');
            message.html('<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> Ожидание внешней аудиодорожки');
            message.css('display', 'block');
          }

          self.tmp.ext_audio_el.on("seeked", function(){

            this.emit("seek_sync");

          }).on("seek_sync", function() {
            if(audioMutex){
              //self.tmp.ext_audio_el.audio.play();
              audioMutex = false;
              //self.tmp.ext_audio_el.play();
              self.videoJS.play();
              log('popcornJS:: "seeked" mutex fired');
              $('.vjs-push-message').css('display', 'none');
            }
          });
          // when each is ready... 
          self.tmp.ext_audio_el.on("canplayall", function() {
        
            // trigger a custom "sync" event
            this.emit("sync");
        
            // Listen for the custom sync event...    
          }).on("sync", function() {

            // Uncomment this line to silence the video
            if(self.ext_audio > 0)
              self.videoJS.muted(true);
            // Iterate all events and trigger them on the video 
            // whenever they occur on the audio
            events.forEach(function(event) {
      
              self.videoJS.on(event, function() {
                
                if(self.ext_audio == 0)
                  return;
                
                // Avoid overkill events, trigger timeupdate manually
                if (event === "timeupdate") {
      
                  if (!self.videoJS.paused()) {
                    var videoTime = self.videoJS.currentTime(),
                      audioTime = self.tmp.ext_audio_el.currentTime(),
                      desync = Math.abs(videoTime - audioTime),
                      time = (new Date).getTime();
                     
                    //console.log('timeupdate:', time);
                    if (desync >= 0.164) { // 2s... sick =_=
                      if((videoTime == lasttime) && !self.tmp.ext_audio_el.paused()) //video hard interrupted
                        {self.tmp.ext_audio_el.pause(); log('popcornJS:: video interrupted');}
                      else if ((videoTime != lasttime) && self.tmp.ext_audio_el.paused()) //video uninterrupted
                        {self.tmp.ext_audio_el.play(videoTime); log(undefined, desync);}
                      else if((videoTime != lasttime) && !self.tmp.ext_audio_el.paused()) //soft desync
                      {
                        if(audioTime == audioLasttime){
                          audioMutex = true;
                          self.videoJS.pause();
                          log('popcornJS:: correction attempt, but audio interrupted');
                          push_message(0);
                        } 
                        else if (((time - self.tmp.lastcorrectiontime) >= 2000) || (desync >= 2000)){
                          self.tmp.ext_audio_el.currentTime(videoTime); 
                          self.tmp.lastcorrectiontime = time;
                          log(undefined, desync);
                        }
                      }
                    }
                    lasttime = videoTime;
                    audioLasttime = audioTime;
                    return;
                  }
                  self.tmp.ext_audio_el.emit("timeupdate");
      
                  return;
                }
      
                if (event === "seeking") {
                    
                    self.tmp.ext_audio_el.pause();
                    self.tmp.ext_audio_el.currentTime(this.currentTime());
                }
                
                if (event === "volumechange") {  
                  /*if(self.videoJS.muted()) { 
                    self.tmp.ext_audio_el.unmute(); 
                    self.tmp.ext_audio_el.volume(self.volume);
                  } else {
                    self.tmp.ext_audio_el.mute(); 
                  } */
                  
                  //self.tmp.ext_audio_el.audio.volume(this.volume());
                }
      
                if (event === "play" || event === "pause") {
                  self.tmp.ext_audio_el[event]();
                  log('popcornJS:: ' + event + '() sync');
                }
              });
            });
          });
        
        }
      });
    }      
  }
  if(this.music)
    $(this.container + ' > audio').css({
      "z-index" : "1",
      "position": "relative",
      "width"   : "640px",
      "top"     : '0px', 
      "display" : "block!important"
    });
  $("#download-chapter").css("background","rgb(255, 153, 0)");
  $("#download-chapter").attr('href',this.downloadCh());
  $(this.container).volume = this.volume;
  /*if(this.music){
    if(this.ids[index].id3 != undefined){
      $('#ost-title').html(this.ids[index].id3.tags.title?this.ids[index].id3.tags.title[0]:'&nbsp');
      $('#ost-artist').html(this.ids[index].id3.tags.artist?this.ids[index].id3.tags.artist[0]:'&nbsp');
      $('#ost-album').html(this.ids[index].id3.tags.album?this.ids[index].id3.tags.album[0]:'&nbsp');
      $('#ost-year').html(this.ids[index].id3.tags.year?this.ids[index].id3.tags.year[0]:'&nbsp');
      $('#ost-genre').html(this.ids[index].id3.tags.genre?this.ids[index].id3.tags.genre[0]:'&nbsp');
      let bitrate = (this.ids[index].id3.bitrate != undefined)?Math.round(this.ids[index].id3.bitrate / 1000).toString()+' [':'&nbsp';
      bitrate += this.ids[index].id3.audio.bitrate_mode?this.ids[index].id3.audio.bitrate_mode:'';
      bitrate += ']'
      $('#ost-bitrate').html(bitrate);
    };
  };*/
  if(this.music)
    this.addEventListener(this.container + ' > audio');
  //this.firstShow = false;
};

player.addEventListener = function (selector) {
  var element = $(selector).get(0);
  if (element != undefined) {
    var self = player;
    element.addEventListener("ended", function () {
      $('#serial_selector > li.pcurrent').next().click();
    });
    element.addEventListener("play", function () {
      self.firstShow = false;
    });
    element.onvolumechange = function () {
      self.volume = element.volume;
    };
    
    element.addEventListener('error', function failed(e) {
      // audio playback failed
      switch (e.target.error.code) {
        case e.target.error.MEDIA_ERR_ABORTED:
          console.log('animeplayer:: audio playback has been aborted');
          break;
        case e.target.error.MEDIA_ERR_NETWORK:
          console.log('animeplayer:: A network error caused the audio download to fail.');
          break;
        case e.target.error.MEDIA_ERR_DECODE:
          console.log('animeplayer:: audio data is corrupted, skipping...');
          $('#serial_selector > li.pcurrent').next().click();
          break;
        case e.target.error.MEDIA_ERR_SRC_NOT_SUPPORTED:
          //console.log('animeplayer:: audio format not supported, skipping...');
          break;
        default:
          console.log('animeplayer:: An unknown playback error occurred.');
          break;
      }
    }, true);
       
    element.volume = self.volume;
    if (!self.firstShow) 
      element.play();
  }
};

player.downloadCh = function(){
  if(this.Sources[0])
    return this.Sources[0].file;
  return '';
};

player.downloadAll = function(){
  if(this.downloadurl)
    window.open(data_domain + this.downloadurl, "_blank");
};

player.clearcache = function(){
  var self = this;
  if(this.music){
    if(this.folders.length > 0){
      $('#cache-dialog').html('Вы действительно хотите очистить кеш?');
      $('#cache-dialog').dialog({
        title: 'Очистка кеша плеера',
        buttons:[            
          {
            text: 'Nah',
            icons: {
              primary: 'ui-icon-close'
            },
            click: function(){
              $(this).dialog('close');
            } 
          },{
            text: 'Да',
            icons: {
              primary: 'ui-icon-trash'
            },
            click: function(){
              let msg = '';
              for(let id = 0; id < self.folders.length; id++){
                  let url = "/phpscript/player.scandir.php?url="+self.folders[id]+"&id="+id+(self.music?"&music=true":"")+'&user=' + self.user + '&clearcache=true'; 
                	$.ajax({
                		type:  "GET",
                		url:   url,
                		async: false,
                		success : function(data) {
                        data = jQuery.parseJSON(data);
                    		msg += data.msg?data.msg + '<br />':'';
                		}
            		  });        
              };
              self.folders = {};
              $('#cache-dialog').html(msg);
              $('#cache-dialog').dialog({
                title: 'Очистка кеша плеера',
                width: 600,
                buttons:[{
                  text: 'Ок',
                  icons: {
                    primary: 'ui-icon-close'
                  },
                  click: function(){
                    $(this).dialog('close')
                  }
                }]
              });               
            }
          }
        ]
      });
    }
    else {
      $('#cache-dialog').html('Кеш уже пуст.');
      $('#cache-dialog').dialog({
        title: 'Очистка кеша плеера',
        buttons:[{
          text: 'Ок',
          icons: {
            primary: 'ui-icon-close'
          },
          click: function(){
            $(this).dialog('close')
          }
        }]
      });        
    };   //лесенка ^_^  
  };
};

//#begin

player.install = function(container, wrapper, show, music, popup, usefilenames){ 
  this.folders = [];
  this.regex = {};
  this.id3 = {};
  this.regex.testsect = /(^"|"$)/g;
  this.regex.grabURI = new RegExp(".*\.(flv|mp4|mp3)");
  this.regex.section_descriptor = /^"(.*)"$/;
  this.regex.disabledch = /^\*\*\*/;
  this.regex.notEmpty = new RegExp(".+");
  this.regex.escape = /^(http:\/\/[^\/]*)(\/.*?)$/;
  this.regex.youtube = /^[a-zA-Z0-9\-]{3,20}$/;
  this.regex.blive = /^[0-9]{1,10}$/;
  this.youtube_embedURI = "http://www.youtube.com/embed/";
  this.blive_embedURI = "http://www.blive.kg/getvideo:";
  this.music = music;
  this.container = container;
  this.wrapper = wrapper;
  this.doshow = show;
  this.usefilenames = usefilenames;
  this.popup = (popup != undefined)?popup:false;
  this.firstShow = true;
  this.Sources = new Array();
  this.section = "";
  this.volume = 1.0;
  this.loadingProgress = 0.0;
  this.currentChapterIdx = 0;
  this.last_chapter = 0;
  this.windowScroll = $(window).scrollTop();
  this.animetitle = $('#video-ids').prop('title');
  this.user = $('#video-ids').attr('user');
  this.themepath = $('#video-ids').attr('themepath');
  this.showfull = $('#video-ids').attr('showfull') == true; 
  this.news_id = $('#video-ids').attr('news_id');
  this.tags = $('#video-ids').attr('tags').split(',');
  this.ongoing = (this.tags.indexOf('ongoing') != -1) && !this.music;
  this.errors = "";
  this.videoJS = null;
  this.enable_sub = false;
  this.ext_audio = 0;
  this.tmp = {};
  this.tmp.vjs_audiobtns = {};
  this.tmp.ext_audio_el = null;
  this.tmp.lastcorrectiontime = 0;
  if(this.doshow && this.popup){
    $(this.wrapper).detach().appendTo('body');
	  $(this.wrapper).css({
                "display"     : "table", 
                "position"    : "fixed", 
                "z-index"     : "61", 
                "top"         : ($(window).height() - $(player.wrapper).height()) / 2 + "px", 
                "left"        : ($(window).width()  - $(player.wrapper).width() ) / 2 + "px", 
                "background"  : "black"
    });
    $('.wrapper, .footer, #header_bg').each(function(){
      $(this).css({
          'display':'none'
      });
    });
    $(window).resize(() => {
        $(this.wrapper).css({
          "top" : ($(window).height() - $(this.wrapper).height()) / 2 + "px", 
          "left": ($(window).width()  - $(this.wrapper).width() ) / 2 + "px", 
        })
    });
  };
  if (this.doshow) {
    $("#load-indicator").nyanBar({
      charSize: 50,
      updatePeriod: 50,
      patterns: ["{-|_}*,------,  ", "{_|-}*|   /\\\\_/\\\\", "{-|_}*|__( ^ .^)", "{_|-}* {{\"| }}\"{{ |\"}} {{\"| }}\"{{ |\"}}  "],
      showProgress: true, // Whether to show a percentage.
      progressFunction: (function() {
        return function() {
          return player.loadingProgress;
        }
      })(), // The function that is queried for progress.
      doneFunction: (function () {
        return true;
      })()
    });
  };
  this.animelistp();
}; 

player.error_out = function(message){
    var errors = `
      <h1>Error</h1>
      <p>${message}</p>
    `;
    errors = errors.replace(/\/n/gm,'<br>');
    $(self.container).html(errors);
}

player.animelistp_onAjaxSuccess = function(ids){
  var self = this;
  self.ids = ids;
  if(self.errors != ""){
    self.error_out(self.errors);
    return false;
  }
  if(ids == ""){
    $('.watch_button_info, #download_button_info_a').each(function(){
      $(this).css({'background' : '#757575', 'display' : 'none'});
    });
    $('.button_block_info').css({'padding' : '52px 0px 0px 0px'});
    return false;
  }
  self.chapters = new Array(self.ids.length);
  if(self.doshow){
    //$(self.wrapper).css('display', self.popup?'none':'table');
    $(self.wrapper).css('display', 'table');
    if(self.music)
      $(self.container).css({'background-image' : 'url(' + self.themepath + '/img/ostplayerbg.jpg' + ')', 'background-position' : '50% 50%', 'background-repeat' : 'no-repeat'});
    $(self.container).children().filter("video").each(function(){
      this.pause(); // can't hurt
      delete this; // @sparkey reports that this did the trick (even though it makes no sense!)
      $(this).remove(); // this is probably what actually does the trick
    });
    $(self.container).children().filter("audio").each(function(){
      this.pause(); // can't hurt
      delete this; // @sparkey reports that this did the trick (even though it makes no sense!)
      $(this).remove(); // this is probably what actually does the trick
    });
    $(this.container).empty();
    let counter = 1;
    for(let i = 0; i < self.chapters.length; i++){
      self.chapters[i] = new Object();
      if(self.regex.testsect.test(self.ids[i]))
      {
        self.chapters[i].configURI = "";
        self.section = self.ids[i].replace(self.regex.testsect,'');  
        self.chapters[i].caption = (self.music || self.usefilenames)?self.section : self.animetitle + ": " + self.section;
        counter = 1;
      }
      else
      {
        if(self.ids[i].path == undefined){
          let tmp = self.ids[i];
          self.ids[i] = {};
          self.ids[i].path = tmp
        }
        if((self.ids[i].path.indexOf('http://') == 0) || (self.regex.grabURI.test(self.ids[i].path))){ 
          //direct link
          if(self.regex.grabURI.test(self.ids[i].path) && (self.ids[i].path.indexOf('http://') != 0))
            self.ids[i].path = data_domain + self.ids[i].path;  
          var _ = self.regex.escape.exec(self.ids[i].path);
          self.chapters[i].configURI = _[1] + _[2].ReplaceMultiple({'#' : '%23', '&' : '%26', '\\?' : '%3F'});
          if(self.ids[i].sub)
            self.chapters[i].subtitle = self.ids[i].sub;
          if(!isEmpty(self.ids[i].ext_audio))
            self.chapters[i].ext_audio = self.ids[i].ext_audio;
        }
        else //for blive.kg
        if(self.regex.blive.test(self.ids[i].path)){
          self.chapters[i].configURI = self.blive_embedURI + self.ids[i].path;
          self.chapters[i].caption = "Серия " + counter + "&nbsp;&nbsp;&nbsp;&nbsp;[blive]";
        }
        else //for youtube.com
        if(self.regex.youtube.test(self.ids[i].path)){
          self.chapters[i].configURI = self.youtube_embedURI + self.ids[i].path;
          self.chapters[i].caption = "Серия " + counter + "&nbsp;&nbsp;&nbsp;&nbsp;[youtube]";
        } 
        else //missing
        if((self.ids[i].path == '') || (self.ids[i].path.indexOf('***') == 0)){       
        
        }        
        else //error
        {
          console.log(`animeplayer:: <b>Не удалось определить тип идентификатора:</b>/n${self.ids[i].path}`);
        }
        if(this.usefilenames){
          if(/([^\/])+$/.exec(self.ids[i].path) == null){
            console.log('animeplayer:: Ошибка! Файл или директория не существует: "'+self.ids[i].path+'"');
          }
          else if(!self.chapters[i].caption)
            self.chapters[i].caption = /([^\/])+$/.exec(self.ids[i].path)[0].replace(/.mp4$/,'').replace(/_/g, ' ');
        }
        else
          self.chapters[i].caption = (!self.section)?(
                                        self.music?(
                                          ((self.ids[i].id3 != undefined) && (self.ids[i].id3.tags != null) && self.ids[i].id3.tags.title)?
                                            self.ids[i].id3.tags.title[0]:
                                          ("Трек " + counter)
                                        ):
                                        ("Серия " + counter)
                                      ) : (
                                        self.section + 
                                        self.music?(
                                          ((self.ids[i].id3 != undefined) && (self.ids[i].id3.tags != null) && self.ids[i].id3.tags.title)?
                                            self.ids[i].id3.tags.title[0]:
                                          self.music?
                                            ("Трек " + counter):
                                            (self.section + " - Серия " + counter)
                                        ):
                                        (" - серия " + counter)
                                      ); // <-- fking shit!
        counter++; 
      }
    };
  };
  switch(self.folders.length){
    case 0:{self.downloadurl = ""; break;}
    case 1:{self.downloadurl = self.folders[0]; break;}
    default: {
      let i = 0,
          i2 = 0,
          i3 = 0,
          minDirLevel = 2147483647,
          levelUpNeeded = false,
          differentDirs = false;

      for(let i = 0; i <= self.folders.length - 2; i++)
        if((self.folders[i]) != (self.folders[i+1]))
          differentDirs = true;
      for(let i = 0; i < self.folders.length; i++){
    		i3 = 0;
    		for(let i2 = 0; i2 < self.folders[i].length; i2++)
    			if(self.folders[i][i2] == '/')
    				i3++;
    		if(minDirLevel == i3)
    			levelUpNeeded = true;
    		if(minDirLevel > i3){
    			minDirLevel = i3;
    			levelUpNeeded = false;
    			self.downloadurl = self.folders[i];
    		}
      };
    	if(levelUpNeeded && differentDirs){ 
    	  if(self.downloadurl[self.downloadurl.length - 1] == '/')
          self.downloadurl = self.downloadurl.substring(0, self.downloadurl.length - 1);
    	  for(let i = self.downloadurl.length - 1; i >= 0; i--)
    		  if(self.downloadurl[i] != '/')
    			  self.downloadurl = self.downloadurl.substring(0, self.downloadurl.length - 1);
    		  else break;
    	};
      break;
    }
  };
  if(self.downloadurl){
    $('#download-all').each(
      function(){
        $(this).css("background","rgb(255, 153, 0)");
      }
    );
    $('.download_button_info').each(
      function(){
        $(this).css({'cursor' : 'pointer'});
      }
    );
    $('#download_button_info_a').each(
      function(){
        $(this).attr('onClick', 'player.downloadAll();');
      }
    );
  }
  else{
    $('.download_button_info').each(
      function(){
        $(this).css({'background-color' : '#757575'});
      }
    );
  }
  $('.watch_button_info').each(
      function(){
          $(this).css({'cursor' : 'pointer'});
          $(this).attr('onClick', "player.install('#player', '.vk_multifilm', true, player.music, !player.showfull, player.usefilenames);");
      }
  );
  if(self.doshow){
  
    self.setChapterSelector();
    
    $('#serial_selector').delegate('li:not(.pcurrent):not(.disabled):not(.section)', 'click', function() {
        $(this).addClass('pcurrent').siblings().removeClass('pcurrent');
        let src = $(this).attr('data-src');
        if ((strpos(src, 'http') === false) && !self.regex.grabURI.test(src)) {
            $(self.container).html('');
        } else {
            $("#download-chapter").css("background","#757575");
            $("#download-chapter").attr('href','javascript: void(0)');
            self.grabURI($(this).attr('data-src'), $(this).attr('index'));
        }
        var top = - $('#serial_selector > li:first-of-type').position().top + $(this).position().top;
        if($(this).position().top > 336)
          $(this).parent().scrollTop(
            top                       - 
            $(this).parent().height() + 
            $(this).outerHeight(true),
          )
        else if($(this).position().top < 0)
          $(this).parent().scrollTop(
            top
          );
    });
    $('#serial_selector').delegate('li.section', 'click', function() {
      self.nextChapter(this);
    });
    /*flowplayer("player", "{THEME}/js/flowplayer/flowplayer-3.2.18.swf",{
      clip:{
        autoPlay: false,
        autoBuffering: true
      }
    });*/
    if((this.user != 'guest') && (this.user != undefined)) {
      $.ajax({
        type:  "GET",
        url:   '/phpscript/player.scandir.php?user='+this.user+'&news_id='+this.news_id+'&last_chapter=true',
        async: !DEBUG,
        success: function(data){
          data = jQuery.parseJSON(data);
          self.last_chapter = parseInt(data.last_chapter);
          $('#serial_selector > li[index="' + self.last_chapter + '"]').click();
        }
      });
    }
    else
      $('#serial_selector > li:first-of-type').click();
  };
};
//#end.
